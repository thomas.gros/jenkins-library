import com.aiconoa.trainings.jenkins.library.EchoProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageProducerTest {

    @Test
    public void echoShouldEcho() {
        String msg = "echo ?";

        EchoProducer echoProducer = new EchoProducer();
        String result = echoProducer.echo(msg);

        assertEquals(msg , result);
    }

    @Test
    public void echoShouldNotAcceptNull() {
        EchoProducer echoProducer = new EchoProducer();
        assertThrows(NullPointerException.class, () -> {
            echoProducer.echo(null);
        } );

    }
}
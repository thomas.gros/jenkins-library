package com.aiconoa.trainings.jenkins.library;

import java.util.Objects;

/**
 * Echo ? Echoooooooooo.
 */
public class EchoProducer {

    /**
     * Echoes a message.
     * @param message the message to echo
     * @return the same message
     */
    public final String echo(final String message) {
        Objects.requireNonNull(message);
        return message;
    }
}
